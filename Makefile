# Top level makefile, the real shit is at src/Makefile , libs, libs/webnode

#https://www.gnu.org/software/make/manual/html_node/Errors.html
#.DELETE_ON_ERROR
#https://www.gnu.org/software/make/manual/html_node/One-Shell.html
#.ONESHELL

default: all

build: default


#-- Init ------------------------------------------------




#-- Tests ------------------------------------------------


clean_test_app:
	cd test/test_app && make clean && rm -rf public/uploads/ && mkdir public/uploads/

build_test_app:
	cd test/test_app && make

check: clean build install clean_test_app build_test_app
	./test/test_app/test_app --config test/test_app/test_app.conf;

valgrind:
	valgrind --tool=memcheck --dsymutil=yes --leak-check=full --show-reachable=yes  ./test/test_app/test_app --config ./test/test_app/test_app.conf

#--------------------------------------------------


#--------------------------------------------------

.DEFAULT:
	cd ./src && $(MAKE) $@
