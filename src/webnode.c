#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/time.h>

sig_atomic_t server_socket_fd;

#include "log.h"
#include "signals.h"
#include "net.h"
#include "request_handler.h"
#include "configuration.h"
#include "action.h"
#include "webnode.h"

int webnode_start(int argc, char *argv[], configure_app_cb config_cb) {
  int new_connection_fd;

  configuration_init(argc, argv);

  if (config_cb != NULL) {
    config_cb();
  }

  server_socket_fd = net_bind_socket();
  setup_signal_listeners(server_socket_fd);

  while(1) {
    new_connection_fd = net_accept_connection();

    if (!fork()) {
      //srand function seeds the random number generator used by rand.
      //This is to ensure that you don't get the same sequence of number every time you run your program.
      srand(getpid());

      //signal(SIGINT, &child_trap);
      struct timeval *start_time = stopwatch_time();

      //debug("%ld.%06ld", start_time->tv_sec, start_time->tv_usec);

      close(server_socket_fd);
      //debug("closed socket ");

      request_handler_handle(new_connection_fd);
      debug(" after req handler handle");

      configuration_free();

      stopwatch_stop(start_time);

      //debug("%ld.%06ld\n", start_time->tv_sec, start_time->tv_usec);

      exit(0);
    }
    /*else {
      err(" handler process fail to fork !");
      printf("fork failed - %d - %s\n", errno, strerror(errno));
      //exit(-1);
    }*/

    close(new_connection_fd);
  }

  return 0;
}

int main(int argc, char *argv[]) {
  fprintf(stdout,"webnode - v0.1 (%s %s)\n", __DATE__, __TIME__);
  return webnode_start(argc, argv, NULL);
}
