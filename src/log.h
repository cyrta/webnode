// based on Zed Show debug macros
// http://c.learncodethehardway.org/book/learn-c-the-hard-waych21.html
#ifndef __dbg_h__
#define __dbg_h__

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

struct timeval *stopwatch_time();
void stopwatch_stop(struct timeval *end_time);

/* The following are definition to colors using ANSI */
/* http://stackoverflow.com/questions/3219393/stdlib-and-colored-output-in-c */

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_GREY    "\x1b[40m"
#define ANSI_GREY_BCK      "\x1b[01;40m"
#define ANSI_COLOR_RESET   "\x1b[0m"
#define ANSI_CLEAR         "\e[1;1H\e[2J"

#define ANSI_UNDERLINE    "\033[4m"
#define ANSI_NO_UNDERLINE "\033[24m"
#define ANSI_ITALIC       "\x1b[03m"
#define ANSI_BOLD         "\033[1m"
#define ANSI_RESET        "\033[22m"
/*
Debug  : 6897BB
Info   : 6A8759
Warn   : BBB529
Error  : FF6B68
Assert : 9876AA
*/


#ifdef NDEBUG
 #define debug(M, ...)
#else
 #define debug(M, ...) fprintf(stderr, "%s[DEBUG][%d] %s:%d: " M "%s\n",ANSI_COLOR_GREY, getpid(), __FILE__, __LINE__, ##__VA_ARGS__,ANSI_COLOR_RESET)
#endif


#define err(M, ...)  fprintf(stderr, "%s[ERROR][%d] (%s:%d) " M "%s\n", ANSI_COLOR_RED, getpid(), __FILE__, __LINE__, ##__VA_ARGS__, ANSI_COLOR_RESET)
#define warn(M, ...) fprintf(stderr, "%s[WARN][%d] (%s:%d) " M "%s\n", ANSI_COLOR_YELLOW, getpid(), __FILE__, __LINE__, ##__VA_ARGS__, ANSI_COLOR_RESET)
#define info(M, ...) fprintf(stderr, "%s[INFO][%d] (%s:%d) " M "%s\n",ANSI_COLOR_GREEN, getpid(), __FILE__, __LINE__, ##__VA_ARGS__, ANSI_COLOR_RESET)

#define die(M, ...)  fprintf(stderr, "%s[FATAL][%d] (%s:%d) " M "%s\n",ANSI_COLOR_RED, getpid(), __FILE__, __LINE__, ##__VA_ARGS__, ANSI_COLOR_RESET); exit(1);

// assert_  ANSI_COLOR_MAGENTA
//verbose  none

#endif
