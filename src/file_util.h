#ifndef _file_util_h
#define _file_util_h

int file_exists(const char *path);
off_t file_size(const char *path);

int file_touch(const char *path);
int file_move(const char *srcpath, const char *destpath);

const char* file_get_extension(const char *path);

#endif
