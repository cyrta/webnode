#FROM progrium/busybox
FROM gcc:latest
MAINTAINER Pawel Cyrta <pawel@cyrta.com>

#RUN opkg-install bash bzip2 

COPY . /usr/src/webnode
WORKDIR /usr/src/webnode
RUN make &&  make install

#CMD ["./src/webnode"]



