#ifndef _webnode_h
#define _webnode_h

#include "http_request.h"
#include "http_response.h"
#include "configuration.h"
#include "render.h"

typedef void (*configure_app_cb) ();

int webnode_start(int argc, char *argv[], configure_app_cb config_cb);

#endif
