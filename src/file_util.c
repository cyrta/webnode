#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h> /* strrchr */

#include "file_util.h"

int file_exists(const char *path) {
  struct stat st;
  return stat(path, &st) == 0 && (st.st_mode & S_IFREG);
}

off_t file_size(const char *path) {
  struct stat st;
  stat(path, &st);
  return st.st_size;
}

int file_touch(const char *path) {
  //http://code.metager.de/source/xref/gnu/coreutils/src/touch.c
  int default_permissions = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;
  //MODE_RW_UGO
  //int fd = open(path, O_RDWR | O_CREAT, S_IRUSR | S_IRGRP | S_IROTH);
  int fd = open(path, O_WRONLY | O_CREAT | O_NONBLOCK | O_NOCTTY, default_permissions);
  if (fd != -1 && errno != EISDIR && errno != EINVAL && errno != EPERM) {
      close(fd);
      return 0;
  }

  return 1;
}

const char *file_get_extension(const char *filename) {
    const char *dot = strrchr(filename, '.');
    if(!dot || dot == filename) return "";
    return dot + 1;
}

int file_move(const char *srcpath, const char *destpath) {
  int status = rename (srcpath, destpath);
  if (status) {
     err(" file moving: something went wrong");
     perror("file moving (rename)");
  }
  return status;
}
