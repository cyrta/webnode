#include <unistd.h>

#include "request_handler.h"
#include "http_request.h"
#include "log.h"

extern int server_socket_fd;

void request_handler_handle(int socket_fd) {
  debug("before http req init");

  http_request *request = http_request_init(socket_fd);

  debug("after http req init (uid: %s)", request->uid);

  http_request_handle(request);

  http_request_free(request);
  close(socket_fd);
}
