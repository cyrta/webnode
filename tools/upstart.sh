#!upstart
description "node.js server"
author      "Pawel Cyrta"

start on startup
stop on shutdown

script
    export HOME="/root"

    echo $$ > /var/run/eutts-speakereval.pid
    exec sudo -u username /usr/local/bin/node /var/www/public/eutts/main.js >> /var/log/eutts-speakereval.sys.log 2>&1
end script

pre-start script
    # Date format same as (new Date()).toISOString() for consistency
    echo "[`date -u +%Y-%m-%dT%T.%3NZ`] (sys) Starting" >> /var/log/eutts-speakereval.sys.log
end script

pre-stop script
    rm /var/run/eutts-speakereval.pid
    echo "[`date -u +%Y-%m-%dT%T.%3NZ`] (sys) Stopping" >> /var/log/eutts-speakereval.sys.log
end script
