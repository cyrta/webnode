

/* the memory failure routine should never return! */
// static void memory_failure(const char*msg) {
void memory_failure(const char*msg) {
  perror(msg);
  exit(EXIT_FAILURE);
};


//malloc ... with
