#include "action.h"
#include "configuration.h"
#include "log.h"
#include "string_util.h"
#include "file_util.h"

#define MAP_CONTENT_TYPE(path, ext, content_type) if (str_ends_with(path, ext)) return strdup(content_type);

static char *content_type_by_path(const char *path) {
  MAP_CONTENT_TYPE(path, "html", "text/html; charset=UTF-8");
  MAP_CONTENT_TYPE(path, "css",  "text/css");
  MAP_CONTENT_TYPE(path, "js",   "application/javascript");

  MAP_CONTENT_TYPE(path, "ico",  "image/x-icon");
  MAP_CONTENT_TYPE(path, "gif",  "image/gif");
  MAP_CONTENT_TYPE(path, "jpg",  "image/jpg");
  MAP_CONTENT_TYPE(path, "png",  "image/png");
  MAP_CONTENT_TYPE(path, "pcm",  "audio/wave; codecs=1");
  MAP_CONTENT_TYPE(path, "mp3",  "audio/mp3"); //mpeg
  MAP_CONTENT_TYPE(path, "m4a",  "audio/mp4");
  MAP_CONTENT_TYPE(path, "aac",  "audio/aac");
  MAP_CONTENT_TYPE(path, "ogg",  "audio/ogg");
  MAP_CONTENT_TYPE(path, "wav",  "audio/wav");
  MAP_CONTENT_TYPE(path, "flac", "audio/flac");

  MAP_CONTENT_TYPE(path, "mp4",  "video/mp4");
  MAP_CONTENT_TYPE(path, "mov",  "video/quicktime");
  MAP_CONTENT_TYPE(path, "avi",  "video/x-msvideo");
  MAP_CONTENT_TYPE(path, "ogv",  "video/ogg");
  MAP_CONTENT_TYPE(path, "webm", "video/webm");

  MAP_CONTENT_TYPE(path, "txt",  "text/plain");
  MAP_CONTENT_TYPE(path, "sbv",  "text/plain");
  MAP_CONTENT_TYPE(path, "srt",  "text/plain");
  MAP_CONTENT_TYPE(path, "vtt",  "text/vtt");
  MAP_CONTENT_TYPE(path, "seg",  "text/plain");
  MAP_CONTENT_TYPE(path, "csv",  "text/csv");
  MAP_CONTENT_TYPE(path, "tsv",  "text/tab-separated-values");

  return strdup("application/octet-stream");
}

void render_file(http_response *response, const char *path) {
  char *relative_file_path = configuration_convert_path_to_local(path);

  response->code = 200;
  response->header_summary = "OK";
  response->file_path = relative_file_path;
  response->content_length = file_size(relative_file_path);
  response->content_type = content_type_by_path(path);
}

void render_data(http_response *response, const char *path) {
  char *relative_file_path = configuration_convert_path_to_local_data(path);
  debug(" rendering file: %s", relative_file_path);

  response->code = 200;
  response->header_summary = "OK";
  response->file_path = relative_file_path;
  response->content_length = file_size(relative_file_path);
  response->content_type = content_type_by_path(path);
}


void render_text(http_response *response, const char *text) {
  response->code = 200;
  response->header_summary = "OK";
  response->file_path = NULL;
  response->content_length = strlen(text);
  response->content_type = strdup("text/html");
  response->raw_response = strdup(text);
}

void render_json(http_response *response, const char *json_string) {
  response->code = 200;
  response->header_summary = "OK";
  response->file_path = NULL;
  response->content_length = strlen(json_string);
  response->content_type = strdup("application/json");
  response->raw_response = strdup(json_string);
}

void render_empty(http_response *response) {
  response->code = 200;
  response->header_summary = "OK";
  response->file_path = NULL;
  response->content_length = 0;
  response->content_type = strdup("text/plain");
  // response->raw_response = 0;
}
